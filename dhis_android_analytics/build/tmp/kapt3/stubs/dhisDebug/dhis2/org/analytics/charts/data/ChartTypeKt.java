package dhis2.org.analytics.charts.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\u0012\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007\u00a8\u0006\b"}, d2 = {"toAnalyticsChartType", "Ldhis2/org/analytics/charts/data/ChartType;", "Lorg/hisp/dhis/android/core/settings/ChartType;", "toNutritionChartType", "Ldhis2/org/analytics/charts/data/NutritionChartType;", "Lorg/hisp/dhis/android/core/settings/WHONutritionChartType;", "isFemale", "", "dhis_android_analytics_dhisDebug"})
public final class ChartTypeKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final dhis2.org.analytics.charts.data.ChartType toAnalyticsChartType(@org.jetbrains.annotations.NotNull()
    org.hisp.dhis.android.core.settings.ChartType $this$toAnalyticsChartType) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final dhis2.org.analytics.charts.data.NutritionChartType toNutritionChartType(@org.jetbrains.annotations.NotNull()
    org.hisp.dhis.android.core.settings.WHONutritionChartType $this$toNutritionChartType, boolean isFemale) {
        return null;
    }
}