package dhis2.org.analytics.charts.mappers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0007\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"DEFAULT_ANIM_TIME", "", "DEFAULT_CHART_HEIGHT", "DEFAULT_GRANULARITY", "", "DEFAULT_GRID_LINE_LENGTH", "DEFAULT_GRID_SPACE_LENGTH", "DEFAULT_GRIP_PHASE", "DEFAULT_VALUE", "VALUE_PADDING", "X_AXIS_DEFAULT_MIN", "dhis_android_analytics_dhisDebug"})
public final class GraphToLineChartKt {
    public static final float DEFAULT_VALUE = 0.0F;
    public static final float VALUE_PADDING = 50.0F;
    public static final float DEFAULT_GRID_LINE_LENGTH = 10.0F;
    public static final float DEFAULT_GRID_SPACE_LENGTH = 10.0F;
    public static final float DEFAULT_GRIP_PHASE = 0.0F;
    public static final int DEFAULT_ANIM_TIME = 1500;
    public static final float DEFAULT_GRANULARITY = 1.0F;
    public static final float X_AXIS_DEFAULT_MIN = -1.0F;
    public static final int DEFAULT_CHART_HEIGHT = 500;
}