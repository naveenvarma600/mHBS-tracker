package dhis2.org.analytics.charts.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b\u00a8\u0006\t"}, d2 = {"Ldhis2/org/analytics/charts/data/NutritionChartType;", "", "(Ljava/lang/String;I)V", "WHO_WFA_BOY", "WHO_WFA_GIRL", "WHO_HFA_BOY", "WHO_HFA_GIRL", "WHO_WFH_BOY", "WHO_WHO_WFH_GIRL", "dhis_android_analytics_dhisDebug"})
public enum NutritionChartType {
    /*public static final*/ WHO_WFA_BOY /* = new WHO_WFA_BOY() */,
    /*public static final*/ WHO_WFA_GIRL /* = new WHO_WFA_GIRL() */,
    /*public static final*/ WHO_HFA_BOY /* = new WHO_HFA_BOY() */,
    /*public static final*/ WHO_HFA_GIRL /* = new WHO_HFA_GIRL() */,
    /*public static final*/ WHO_WFH_BOY /* = new WHO_WFH_BOY() */,
    /*public static final*/ WHO_WHO_WFH_GIRL /* = new WHO_WHO_WFH_GIRL() */;
    
    NutritionChartType() {
    }
}