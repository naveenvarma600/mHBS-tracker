package dhis2.org.analytics.charts.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u001e\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\b\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"nutritionTestingData", "", "Ldhis2/org/analytics/charts/data/Graph;", "d2", "Lorg/hisp/dhis/android/core/D2;", "dhis_android_analytics_dhisDebug"})
public final class NutritionTestingDataKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final java.util.List<dhis2.org.analytics.charts.data.Graph> nutritionTestingData(@org.jetbrains.annotations.NotNull()
    java.util.List<dhis2.org.analytics.charts.data.Graph> $this$nutritionTestingData, @org.jetbrains.annotations.NotNull()
    org.hisp.dhis.android.core.D2 d2) {
        return null;
    }
}