package dhis2.org.analytics.charts.mappers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0004\u00a8\u0006\n"}, d2 = {"Ldhis2/org/analytics/charts/mappers/GraphCoordinatesToBarEntry;", "", "()V", "map", "", "Lcom/github/mikephil/charting/data/BarEntry;", "graph", "Ldhis2/org/analytics/charts/data/Graph;", "coordinates", "Ldhis2/org/analytics/charts/data/GraphPoint;", "dhis_android_analytics_dhisDebug"})
public final class GraphCoordinatesToBarEntry {
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.github.mikephil.charting.data.BarEntry> map(@org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.data.Graph graph, @org.jetbrains.annotations.NotNull()
    java.util.List<dhis2.org.analytics.charts.data.GraphPoint> coordinates) {
        return null;
    }
    
    public GraphCoordinatesToBarEntry() {
        super();
    }
}